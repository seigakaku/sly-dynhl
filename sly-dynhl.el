;;; sly-treesit-dynamic-highlight.el --- sly+treesitter dynamic syntax higlighting for common lisp  -*- lexical-binding: t; -*-

;; Author: Seiga Kaku <iosevka@naver.com>
;; URL: https://codeberg.org/seigakaku/myougiden.el
;; Created: 2023
;; Version: 0.0.1
;; Keywords: sly treesitter common-lisp
;; Dependencies: sly, treesit

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; This file is NOT part of GNU Emacs.

;;; Code:

(defface sly-dynhl-bracket-face '((t :inherit font-lock-bracket-face)) "Parentheses")

(defface sly-dynhl-builtin-face '((t :inherit font-lock-function-name-face)) "Special builtin language constructs")

(defface sly-dynhl-function-name-face '((t :inherit font-lock-function-name-face)) "Defun names")
(defface sly-dynhl-function-call-face '((t :inherit font-lock-function-call-face)) "Function calls")

(defface sly-dynhl-macro-name-face '((t :inherit sly-dynhl-function-call-face)) "Defmacro names")
(defface sly-dynhl-macro-call-face '((t :inherit sly-dynhl-function-call-face)) "Macro calls")

(defface sly-dynhl-method-name-face '((t :inherit sly-dynhl-function-name-face)) "Defmethod names")
(defface sly-dynhl-method-call-face '((t :inherit sly-dynhl-function-call-face)) "Method calls")
(defface sly-dynhl-generic-name-face '((t :inherit sly-dynhl-method-name-face)) "Defgeneric names")

(defface sly-dynhl-special-name-face '((t :inherit font-lock-variable-name-face)) "Special variable definition names")
(defface sly-dynhl-special-use-face '((t :inherit font-lock-variable-use-face)) "Special variable use")

(defface sly-dynhl-constant-name-face '((t :inherit font-lock-constant-face)) "Constant variable definition names")
(defface sly-dynhl-constant-use-face '((t :inherit font-lock-constant-face)) "Constant variable use")

(defface sly-dynhl-compiler-macro-name-face '((t :inherit sly-dynhl-macro-name-face)) "Hhe name of compiler macros (define-compiler-macro)")

(defface sly-dynhl-ampersand-symbol-face '((t :inherit font-lock-builtin-face)) "Ampersand prefixed symbols as in lambda lists")
(defface sly-dynhl-character-face '((t :inherit font-lock-constant-face)) "Character reader macros #\\Char")
(defface sly-dynhl-string-face '((t :inherit font-lock-string-face)) "Strings \"string\"")
(defface sly-dynhl-pathname-face '((t :inherit font-lock-string-face)) "Pathames #P\"path\"")
(defface sly-dynhl-pathname-reader-macro-face '((t :inherit sly-dynhl-pathname-face)) "Pathame reader macro \"#P\"")
(defface sly-dynhl-number-face '((t :inherit font-lock-number-face)) "Numbers 1 2 3")
(defface sly-dynhl-nil-face '((t :inherit font-lock-variable-use-face)) "Boolean nil")
(defface sly-dynhl-t-face '((t :inherit font-lock-variable-use-face)) "Boolean t")

(defface sly-dynhl-keyword-face '((t :inherit font-lock-builtin-face)) "Keyword symbols :keyword")
(defface sly-dynhl-uninterned-face '((t :inherit font-lock-builtin-face)) "Uninterned symbols #:symbol")

(defface sly-dynhl-comment-face '((t :inherit font-lock-comment-face)) "Comments ;;; My Comment")
(defface sly-dynhl-block-comment-face '((t :inherit sly-dynhl-comment-face)) "Comments ;;; My Comment")
(defface sly-dynhl-doc-face '((t :inherit font-lock-doc-face)) "Documentation strings")

(defface sly-dynhl-defpackage-package-face '((t :inherit sly-dynhl-builtin-face)) "Package names in defpackage")
(defface sly-dynhl-defpackage-nickname-face '((t :inherit sly-dynhl-keyword-face)) "Package nicknames in defpackage")
(defface sly-dynhl-defpackage-symbol-face '((t :inherit sly-dynhl-keyword-face)) "Package symbols in defpackage")

(defface sly-dynhl-type-face '((t :inherit font-lock-type-face)) "Types fixnum simple-string")
(defface sly-dynhl-loop-keyword-face '((t :inherit font-lock-builtin-face)) "Loop macro keywords (for with collect do)")

(defface sly-dynhl-declamation-face '((t :inherit font-lock-builtin-face)) "Declaim/proclaim/declare policy keywords (ftype type)")
(defface sly-dynhl-optimization-level-0-face '((t :inherit font-lock-number-face)) "Declaim/proclaim/declare optimize policy level 0")
(defface sly-dynhl-optimization-level-1-face '((t :inherit font-lock-number-face)) "Declaim/proclaim/declare optimize policy level 1")
(defface sly-dynhl-optimization-level-2-face '((t :inherit font-lock-number-face)) "Declaim/proclaim/declare optimize policy level 2")
(defface sly-dynhl-optimization-level-3-face '((t :inherit font-lock-number-face)) "Declaim/proclaim/declare optimize policy level 3")

(defface sly-dynhl-quote-face '((t :inherit default)) "Quoting face (')")
(defface sly-dynhl-function-quote-face '((t :inherit sly-dynhl-quote-face)) "Function quote face (#')")
(defface sly-dynhl-backquote-face '((t :inherit sly-dynhl-quote-face)) "Backquote face (`)")
(defface sly-dynhl-unquote-face '((t :inherit default)) "Unquote face (, and ,.)")
(defface sly-dynhl-splicing-unquote-face '((t :inherit sly-dynhl-unquote-face)) "Splicing unquote face (,@)")

(defface sly-dynhl-lambda-list-optional-face '((t :inherit sly-dynhl-variable-name-face)) "Optional variables in lambda lists")
(defface sly-dynhl-lambda-list-keyword-face '((t :inherit sly-dynhl-keyword-face)) "Keyword variables in lambda lists")
(defface sly-dynhl-lambda-list-rest-face '((t :inherit sly-dynhl-variable-name-face)) "&rest variable in lambda list")
(defface sly-dynhl-lambda-list-body-face '((t :inherit sly-dynhl-variable-name-face)) "&body variable in lambda list")
(defface sly-dynhl-lambda-list-whole-face '((t :inherit sly-dynhl-keyword-face)) "&whole variable in lambda list")

;;;; ==================================================
;;;; Finding the symbols

(let ((table `(
               :variables boundp
               :functions fboundp
               :methods (lambda (s) (ignore-errors (typep (symbol-function s) 'generic-function)))
               :macros macro-function
               :types (lambda (s) (and (uiop:featurep :sbcl)
                                       (uiop:symbol-call :sb-ext :valid-type-specifier-p s))))))

  (defun sly-dynhl--collect-symbols (pkgname predicate)
    (let ((cl `(let* ((pkg (find-package ,pkgname))
                      (pkg-symbols
                       (lambda (pkg)
                         (loop for sym being the symbols of pkg
                               when (funcall #',predicate sym)
                               collect (string-downcase sym)))))
                 (declare (optimize (speed 3) (safety 0)))
                 (when pkg
                   (append (funcall pkg-symbols pkg)
                           (mapcan (lambda (ln)
                                     (funcall pkg-symbols (cdr ln)))
                                   (package-local-nicknames pkg)))))))
      (sly-eval `(cl:eval (cl:read-from-string ,(prin1-to-string cl))) "CL-USER")))

  (defun sly-dynhl--build-regex-table (package)
    (clrhash *sly-dynhl--rx-table*)
    (cl-loop
     for (type predicate) on table by #'cddr
     do (puthash type
                 (regexp-opt (sly-dynhl--collect-symbols package predicate) 'symbols)
                 *sly-dynhl--rx-table*))))

;;;; ==================================================
;;;; Expanding our grammar definition

(let ((table '(("@fl-" . "@font-lock-%s-face")
               ("@hl-" . "@sly-dynhl-%s-face"))))
  (defun sly-dynhl--expand-face-symbol (sym)
    (cl-flet ((prefixp (str prefix) (s-starts-with-p prefix str)))
      (if-let* ((name (symbol-name sym))
                (match (cl-find name table :test #'prefixp :key #'car)))
          (cl-destructuring-bind (short . fmt) match
            (make-symbol (format fmt (substring name (length short)))))
        sym))))

(defun sly-dynhl--expand-faces (tree)
  (cl-labels ((tree-map (fn tree)
                (when tree
                  (if (atom tree)
                      (funcall fn tree)
                    (cons (tree-map fn (car tree))
                          (tree-map fn (cdr tree)))))))
    (tree-map (lambda (sym)
                (if (symbolp sym)
                    (sly-dynhl--expand-face-symbol sym)
                  sym))
              tree)))

(defmacro sly-dynhl--expand-query (query)
  (sly-dynhl--expand-faces query))

;;;; ==================================================

(put 'sly-dynhl--treesit-font-lock-rules* 'lisp-indent-function 'defun)
(defmacro sly-dynhl--treesit-font-lock-rules* (opts &rest rules)
  `(treesit-font-lock-rules
    ,@(cl-loop
       for (feature query) on rules by #'cddr
       nconc `(,@opts :feature ',feature (sly-dynhl--expand-query ,query)))))

;; (defun sly-dynhl-defun (keyword fname-tag)
;;   `(defun (defun_header keyword: (defun_keyword ,(car keyword)) (cadr keyword)
;;                         :anchor ,(sly-dynhl--symbol fname-tag))))

(defmacro sly-dynhl--sym (tag)
  `'([(sym_lit) ,tag (package_lit symbol: (sym_lit) ,tag)] ,tag))

(defun sly-dynhl--symf (tag)
  `([(sym_lit) ,tag (package_lit symbol: (sym_lit) ,tag)] ,tag))

(defun sly-dynhl--pkg ()
  `[(sym_lit) (str_lit) (kwd_lit) "#"])

(defun sly-dynhl--rx (&rest symbols)
  (regexp-opt symbols 'symbols))

(defun sly-dynhl--defun (keyword name-face)
  `(defun_header keyword: (defun_keyword ,keyword)
                 :anchor [,(sly-dynhl--symf name-face)
                          (list_lit :anchor
                                    ,(sly-dynhl--symf '@sly-dynhl-builtin-face)
                                    ,(sly-dynhl--symf name-face)
                                    :anchor)]))

(defun sly-dynhl--match= (syms tag)
  (if (listp syms)
      `(:match ,(apply #'sly-dynhl--rx syms) ,tag)
    `(:equal ,syms ,tag)))

(defun sly-dynhl--decl (declaimer declamation body)
  `((list_lit :anchor ,@(sly-dynhl--sym @sly-dynhl-macro-call-face)
              [(quoting_lit marker: _
                            (list_lit :anchor (sym_lit) @sly-dynhl-declamation-face
                                      ,@body))
               (list_lit :anchor (sym_lit) @sly-dynhl-declamation-face
                         ,@body)])
    ,(sly-dynhl--match= declaimer '@sly-dynhl-macro-call-face)
    ,(sly-dynhl--match= declamation '@sly-dynhl-declamation-face)))

(defun sly-dynhl--decl-optimize (&rest faces)
  (cl-loop
   for (level face) on faces by #'cddr
   collect (sly-dynhl--decl '("declare" "declaim" "proclaim") "optimize"
                            `((list_lit :anchor
                                        (sym_lit) @sly-dynhl-optimization-policy-face
                                        (num_lit) ,face
                                        :anchor)
                              (:equal ,level ,face)))))

(defun sly-dynhl--ll-ampersand-predicate (ll ampersand node &optional outer)
  (let* ((ll (treesit-node-children ll))
         (pos (cl-position (or outer node) ll :test #'treesit-node-eq))
         (start (cl-position ampersand ll :test #'treesit-node-eq))
         (end (cl-position-if (lambda (text) (s-starts-with-p "&" text)) ll
                              :key #'treesit-node-text :start (1+ start))))
    (or (null end) (and (>= pos start) (< pos end)))))

(defun sly-dynhl--ll-ampersand (ampersand-sym face)
  `(((defun_header (list_lit (sym_lit) @amp-sym (sym_lit) ,face) @ll :anchor)
     (:equal ,ampersand-sym @amp-sym)
     (:pred sly-dynhl--ll-ampersand-predicate @ll @amp-sym ,face))

    ((defun_header (list_lit (sym_lit) @amp-sym (list_lit :anchor (sym_lit) ,face) @outer) @ll :anchor)
     (:equal ,ampersand-sym @amp-sym)
     (:pred sly-dynhl--ll-ampersand-predicate @ll @amp-sym ,face @outer))))

(defun sly-dynhl--ll-ampersand-last (ampersand-sym face)
  `((defun_header (list_lit (sym_lit) @amp-sym :anchor (sym_lit) ,face :anchor) :anchor)
    (:equal ,ampersand-sym @amp-sym)))

(defun sly-dynhl--defpackge (keywords predicates)
  `((list_lit ,(sly-dynhl--sym @sly-dynhl-macro-call-face)
              :anchor ,(sly-dynhl--pkg) @sly-dynhl-defpackage-package-face
              ,keywords)
    (:match "\\_<\\(def\\(?:\\(?:ine-\\)?package\\)\\)\\_>" @sly-dynhl-macro-call-face)
    ,@predicates))

(defvar *sly-dynhl--rx-table* (make-hash-table :test 'eq))

(defun sly-dynhl--generate-treesit-rules ()
  (sly-dynhl--treesit-font-lock-rules* (:language 'commonlisp :override t)
    numbers '((num_lit) @hl-number)
    strings '((str_lit) @hl-string)
    pathnames '(open: "#P" @hl-pathname-reader-macro (path_lit) @hl-pathname)

    declamations `(;; (declare (ignore x y z))
                   ,(sly-dynhl--decl "declare" '("ignore" "ignorable" "dynamic-extent")
                                     (sly-dynhl--sym @hl-variable-name))
                   ;; (declaim/proclaim (inline/notinline x y z))
                   ,(sly-dynhl--decl '("declaim" "proclaim") '("inline" "notinline")
                                     (sly-dynhl--sym @hl-function-name))
                   ;; (declaim/proclaim/declare (type type var var var))
                   ,(sly-dynhl--decl '("declare" "declaim" "proclaim") "type"
                                     '(:anchor (_) @hl-type (_) @hl-variable-name))
                   ;; (declaim/proclaim (ftype (function (type type) ret) fn fn))
                   ,(sly-dynhl--decl '("declaim" "proclaim") "ftype"
                                     '(:anchor (_) @hl-type (_) @hl-function-name))

                   ;; (declaim/proclaim/declare (optimize (policy level) (policy level)))
                   ,@(sly-dynhl--decl-optimize "0" '@hl-optimization-level-0
                                               "1" '@hl-optimization-level-1
                                               "2" '@hl-optimization-level-2
                                               "3" '@hl-optimization-level-3)

                   ;; (deftype type ())
                   ((list_lit :anchor ,(sly-dynhl--sym @hl-macro-call)
                              :anchor ,(sly-dynhl--sym @hl-type))
                    (:equal "deftype" @hl-macro-call))
                   ;; (serapeum:-> fn (type) ret)
                   ((list_lit :anchor ,(sly-dynhl--sym @hl-macro-call)
                              :anchor ,(sly-dynhl--sym @hl-function-name)
                              (list_lit [_ @hl-type (_) @hl-type]))
                    (:equal "->" @hl-macro-call)))

    comments '(;; Comments like ; Comment
               ((comment) @hl-comment)
               ;; Comments like #| Comment |#
               ((block_comment) @hl-block-comment))


    ;; Character reader macros lke '#\SMALL_GREEK_LETTER_LAMDA'
    characters '((char_lit) @hl-character)

    keywords '(;; Standard symbols in the KEYWORD package ':keyword'
               ((kwd_lit) @hl-keyword)
               ;; Uninterned symbols like '#:symbol'
               ("#" @hl-uninterned (kwd_lit) @hl-uninterned))

    packages `(;; Match the package name
               ((list_lit ,(sly-dynhl--sym @hl-macro-call) :anchor ,(sly-dynhl--pkg) @hl-defpackage-package)
                (:match "\\_<\\(def\\(?:\\(?:ine-\\)?package\\)\\)\\_>" @hl-macro-call))
               ;; [shadowing-]import-from
               ,(sly-dynhl--defpackge
                 `(list_lit :anchor (kwd_lit (kwd_symbol) @defpackage-keyword)
                            :anchor ,(sly-dynhl--pkg) @hl-defpackage-package
                            ,(sly-dynhl--pkg) :? @hl-defpackage-symbol)
                 `((:match "\\_<\\(\\(?:shadowing-\\)?import-from\\)\\_>" @defpackage-keyword)))
               ;; export, shadow, intern, unintern
               ,(sly-dynhl--defpackge
                 `(list_lit :anchor (kwd_lit (kwd_symbol) @defpackage-keyword)
                            ,(sly-dynhl--pkg) @hl-defpackage-symbol)
                 `((:match "\\_<\\(export\\|intern\\|shadow\\|unintern\\)\\_>" @defpackage-keyword)))
               ;; [[use|mix]-]reexport, use, mix, recycle
               ,(sly-dynhl--defpackge
                 `(list_lit :anchor (kwd_lit (kwd_symbol) @defpackage-keyword)
                            ,(sly-dynhl--pkg) @hl-defpackage-package)
                 `((:match "\\(mix\\(?:-reexport\\)?\\|re\\(?:cycle\\|export\\)\\|use\\(?:-reexport\\)?\\)"
                           @defpackage-keyword)))
               ;; nicknames
               ,(sly-dynhl--defpackge
                 `(list_lit :anchor (kwd_lit (kwd_symbol) @defpackage-keyword)
                            ,(sly-dynhl--pkg) @hl-defpackage-nickname)
                 `((:equal "nicknames" @defpackage-keyword)))
               ;; local-nicknames
               ,(sly-dynhl--defpackge
                 `(list_lit :anchor (kwd_lit (kwd_symbol) @defpackage-keyword)
                            (list_lit :anchor ,(sly-dynhl--pkg) @hl-defpackage-nickname
                                      ,(sly-dynhl--pkg) @hl-defpackage-package :anchor))
                 `((:equal "local-nicknames" @defpackage-keyword)))
               ;; In-package
               ((list_lit ,(sly-dynhl--sym @hl-macro-call) :anchor
                          ,(sly-dynhl--pkg) @hl-defpackage-package)
                (:equal "in-package" @hl-macro-call)))

    ;; ;; Possibly user defined symbols t
    with-define `((,(sly-dynhl--sym @hl-macro-call)
                   (:match "\\_<\\(with-\\|def\\(ine-\\)?\\).*\\_>" @hl-macro-call)))

    defun `(;; Mark all defun keywords as builtin
            (defun_keyword) @hl-builtin
            ;; Mark all "function names" after keyword as function-name
            (defun_header (defun_keyword _) @hl-builtin
                          :anchor [,(sly-dynhl--sym @sly-dynhl-function-name)
                                   (list_lit :anchor ;; Handle function names as lists
                                             ,(sly-dynhl--sym @sly-dynhl-builtin-face)
                                             ,(sly-dynhl--sym @sly-dynhl-function-name)
                                             :anchor)])

            ;; Override "function name" after some defun keywords
            ,(sly-dynhl--defun "defun" '@hl-function-name)
            ,(sly-dynhl--defun "defmacro" '@hl-macro-name)
            ,(sly-dynhl--defun "defmethod" '@hl-method-name)
            ,(sly-dynhl--defun "defgeneric" '@hl-generic-name)

            ;; Compiler macros
            ((list_lit :anchor ,(sly-dynhl--sym @hl-builtin)
                       :anchor ,(sly-dynhl--sym @hl-compiler-macro-name)
                       :anchor (list_lit (sym_lit) @hl-ampersand-symbol
                                         :anchor (sym_lit) @hl-lambda-list-whole
                                         (sym_lit) @hl-variable-name))
             (:equal "define-compiler-macro" @hl-builtin)
             (:equal "&whole" @hl-ampersand-symbol))

            ((list_lit :anchor ,(sly-dynhl--sym @hl-builtin)
                       :anchor ,(sly-dynhl--sym @hl-compiler-macro-name)
                       :anchor (list_lit (sym_lit) @hl-variable-name)
                       :anchor (str_lit) @hl-doc :?)
             (:equal "define-compiler-macro" @hl-builtin))

            ;; Deftype
            ((list_lit :anchor ,(sly-dynhl--sym @hl-builtin)
                       :anchor ,(sly-dynhl--sym @hl-type)
                       :anchor (list_lit (sym_lit) @hl-variable-name)
                       :anchor (str_lit) @hl-doc :?)
             (:equal "deftype" @hl-builtin))

            ;; Handle lambda lists
            (defun_header (list_lit (sym_lit) @hl-variable-name) :anchor)
            (defun_header (list_lit (list_lit :anchor
                                              (sym_lit) @hl-variable-name ;; LL (&optional (name))
                                              (_) :? ;; LL (&optional (name value))
                                              :anchor (sym_lit) @hl-variable-name :? ;; LL (&optional (name value supplied-p))
                                              :anchor))
                          :anchor)

            ,@(sly-dynhl--ll-ampersand "&optional" '@hl-lambda-list-optional)
            ,@(sly-dynhl--ll-ampersand "&key" '@hl-lambda-list-keyword)

            ,(sly-dynhl--ll-ampersand-last "&rest" '@hl-lambda-list-rest)
            ,(sly-dynhl--ll-ampersand-last "&body" '@hl-lambda-list-body)


            ((defun_header (list_lit (sym_lit) @hl-ampersand-symbol) :anchor)
             (:match "\\_<&.+\\_>" @hl-ampersand-symbol))

            ;; Defun docstrings
            ((defun_header) :anchor (str_lit) @hl-doc))

    loop '((loop_macro (loop_clause (_ :anchor ":" @hl-loop-keyword :?
                                       ["with"
                                        "for" "as" "and"
                                        "do"
                                        "initially" "finally" "return"
                                        "when" "unless" "if" "else"
                                        "while" "until" "repeat"
                                        "always" "never" "thereis"
                                        (accumulation_verb)]
                                       @hl-loop-keyword)))
           (loop_macro (loop_clause (for_clause :anchor
                                                (sym_lit) @hl-variable-name
                                                [(for_clause_word) "and"] @hl-loop-keyword)))
           (loop_macro (loop_clause (accumulation_clause (accumulation_verb)
                                                         ":" @hl-loop-keyword :?
                                                         "into" @hl-loop-keyword
                                                         (sym_lit) @hl-variable-use)))
           (loop_macro (loop_clause (with_clause (sym_lit) @hl-variable-name
                                                 ":" @hl-loop-keyword :?
                                                 "=" @hl-loop-keyword)))
           (loop_macro "loop" @hl-macro-call))

    builtins `(;; #:cl types as per #'sb-ext:valid-type-specifier-p
               (,(sly-dynhl--sym @hl-type)
                (:match ,(gethash :types *sly-dynhl--rx-table*) @hl-type))
               ;; #:cl functions as per #'fboundp
               ((list_lit :anchor ,(sly-dynhl--sym @hl-function-call))
                (:match ,(gethash :functions *sly-dynhl--rx-table*) @hl-function-call))
               ;; #:cl macros as per #'macro-function
               ((list_lit :anchor ,(sly-dynhl--sym @hl-macro-call))
                (:match ,(gethash :macros *sly-dynhl--rx-table*) @hl-macro-call))
               ;; #:cl methods as per (typep 'generic-function)
               ((list_lit :anchor ,(sly-dynhl--sym @hl-method-call))
                (:match ,(gethash :methods *sly-dynhl--rx-table*) @hl-method-call))
               ;; #:cl variables as per #'boundp
               ((,(sly-dynhl--sym @hl-variable-name)
                 (:match ,(gethash :variables *sly-dynhl--rx-table*) @hl-variable-name))))

    documentation `(;; defstruct/defclass/define-constant and etc (:documentation <doc>)
                    ((kwd_lit (kwd_symbol) @hl-keyword)
                     :anchor (str_lit) @hl-doc
                     (:equal "documentation" @hl-keyword)))

    quoting `(((quoting_lit marker: _ @hl-quote))
              ((var_quoting_lit marker: _ @hl-function-quote))
              ((syn_quoting_lit marker: _ @hl-backquote))
              ((unquoting_lit marker: _ @hl-unquote))
              ((unquote_splicing_lit marker: _ @hl-splicing-unquote)))

    ;; Special variables within ** '*special*'
    specials `((,(sly-dynhl--sym @hl-special-use) (:match "\\_<\\*.+\\*\\_>" @hl-special-use)))

    ;; Constants within ++ '+constant-var+'
    constants `((,(sly-dynhl--sym @hl-constant-use) (:match "\\_<\\+.+\\+\\_>" @hl-constant-use)))

    definitions `(;; defglobal has required value, optional doc and makes a simple variable name
                  ((list_lit ,(sly-dynhl--sym @hl-builtin)
                             :anchor (sym_lit) @hl-variable-name
                             (_) (_) @hl-doc :? :anchor)
                   (:equal "defglobal" @hl-builtin))
                  ;; defconstant has required value, optional doc and makes a constant name
                  ((list_lit ,(sly-dynhl--sym @hl-builtin)
                             :anchor (sym_lit) @hl-constant-name
                             (_) (_) @hl-doc :? :anchor)
                   (:equal "defconstant" @hl-builtin))
                  ;; defparameter has required value, optional doc and makes a special name
                  ((list_lit ,(sly-dynhl--sym @hl-builtin)
                             :anchor (sym_lit) @hl-special-name
                             (_) (_) @hl-doc :? :anchor)
                   (:equal "defparameter" @hl-builtin))
                  ;; defvar has optional value and optional doc, it makes a special name
                  ((list_lit ,(sly-dynhl--sym @hl-builtin)
                             :anchor ,(sly-dynhl--sym @hl-special-name)
                             ((_) (_) @hl-doc :? :anchor) :?)
                   (:equal "defvar" @hl-builtin))
                  ;; serapeum:define-constant has required value keyword doc specifier, it makes a constant name
                  ((list_lit ,(sly-dynhl--sym @hl-builtin)
                             :anchor (sym_lit) @hl-constant-name)
                   (:equal "define-constant" @hl-builtin)))

    ;; Match nil and t
    booleans `((,(sly-dynhl--sym @hl-t)
                (:equal "t" @hl-t))
               ((nil_lit) @hl-nil))

    parentheses `(["(" ")"] @hl-bracket)))

(defun sly-dynhl-reload-buffers ()
  (interactive)
  (dolist (buffer (buffer-list))
    (with-current-buffer buffer
      (when (eq major-mode 'common-lisp-ts-dynhl-mode)
        (normal-mode)))))

(defun sly-dynhl-reload ()
  (interactive)
  (sly-dynhl--build-regex-table (sly-dynhl--current-package))
  (sly-dynhl-reload-buffers))

(defun sly-dynhl--current-package ()
  (if-let* ((pkg (sly-current-package)))
      (s-chop-prefix "#:" (upcase pkg))
    "COMMON-LISP-USER"))

;;;###autoload
(define-derived-mode common-lisp-ts-dynhl-mode common-lisp-mode "CL DynHL"
  (when (treesit-ready-p 'commonlisp)
    (treesit-parser-create 'commonlisp)

    (setq treesit-font-lock-feature-list
          '((comments strings keywords defun ampersand with-define builtins booleans packages pathnames)
            (characters loop types declamations documentation definitions quoting)
            (numbers constants specials parentheses)))

    (setq treesit-font-lock-settings (sly-dynhl--generate-treesit-rules))
    (treesit-major-mode-setup)
    (add-hook 'sly-connected-hook #'sly-dynhl-reload)))

(provide 'sly-dynhl)

;;; sly-treesit-dynamic-highlight.el ends here
